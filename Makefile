SHELL := bash
ifeq ($(shell uname),Windows_NT)
    SHELL := bash.exe
endif

.SHELLFLAGS := -euo pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-40s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' Makefile
.DEFAULT_GOAL:=help

##@ [Docker]

IMAGE_NAME = boulzy/php-mapper

.PHONY: build
build: ## Build the Docker image
	@docker build -t $(IMAGE_NAME) .

.PHONY: sh
sh: ## Open a shell in a container
	@docker run --rm -v ./:/app -it $(IMAGE_NAME) sh

##@ [Tests]

.PHONY: test
test: ## Run the test suites
test: phpunit

.PHONY: phpunit
phpunit: ## Run PHPUnit tests
	@docker run --rm -v ./:/app -e XDEBUG_MODE=coverage $(IMAGE_NAME) vendor/bin/phpunit --coverage-text

##@ [Quality Analysis]

.PHONY: qa
qa: ## Run the QA tools
qa: php-cs-fixer phpstan

.PHONY: php-cs-fixer
php-cs-fixer: ## Run PHP CS Fixer
	@docker run --rm -v ./:/app $(IMAGE_NAME) vendor/bin/php-cs-fixer fix

.PHONY: phpstan
phpstan: ## Run PHPStan analysis
	@docker run --rm -v ./:/app $(IMAGE_NAME) vendor/bin/phpstan
