# 🚀 Getting Started

The transformation of an object into another is done in two steps:

1. Providing an instance of the target class.
2. Mapping the properties of the source object to the instance retrieved previously.

The DataTransformer delegates these tasks to two kind of services:

A **Provider** is responsible for loading an instance of the target class. This instance will then be used along with
the source object by a **Mapper** to copy properties from one object to the other. The DataTransformer will then return
the target object.

This library comes with two providers and one mapper:

- `Boulzy\DataTransformer\Provider\NewObjectProvider`: Load a new target by instanciating a new object.
- `Boulzy\DataTransformer\Provider\DoctrineProvider`: Try to load an object managed by Doctrine, or fallback on another
provider.
- `Boulzy\DataTransformer\Mapper\PropertyMapper`: Map two objects properties by matching their name and using the
Symfony PropertyAccess and PropertyInfo components.

We'll see how to use more providers and/or mappers later.

For now, let's retrieve a DataTransformer:

### In a Symfony application

#### 1. Enable the bundle

```php
<?php

// config/bundles.php
return [
    // ...
    Boulzy\DataTransformer\Bundle\BoulzyDataTransformerBundle::class => ['all' => true],
    // ...
];
```

#### 2. Use the DataTransformer service

The DataTransformer is available as a service in the Symfony container.
You can retrieve it using the `Boulzy\DataTransformer\DataTransformerInterface` type-hint, or the
`boulzy_data_transformer.data_transformer` service identifier.

```php
<?php

namespace App\Service;

use App\DTO\User as UserDTO;
use App\Entity\User;
use Boulzy\DataTransformer\Context;use Boulzy\DataTransformer\DataTransformerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class SomeService
{
    public function __construct(
        #[Autowire(service: 'boulzy_data_transformer.data_transformer')]
        private DataTransformerInterface $transformer,
    ) {
    }

    public function doSomething(UserDTO $input): UserDTO
    {
        $user = $this->transformer->transform($input, User::class);

        // Do something with your user

        return $this->transformer->transform($user, UserDTO::class, [Context::INITIAL_DATA => $input]);
    }
}
```

### Manually

```php
<?php

/**
 * @see https://symfony.com/doc/current/components/property_info.html#usage
 */
$propertyInfo = new \Symfony\Component\PropertyInfo\PropertyInfoExtractor(/* ... */);
/**
 * @see https://symfony.com/doc/current/components/property_access.html#usage
 */
$propertyAccessor = \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor();

$provider = new \Boulzy\DataTransformer\Provider\NewObjectProvider();
$mapper = new \Boulzy\DataTransformer\Mapper\PropertyMapper($propertyInfo, $propertyAccessor);
$dataTransformer = new \Boulzy\DataTransformer\DataTransformer($provider, $mapper);
```

## Learn more

If you need to extend the capabilities of the DataTransformer, see the following sections:

- [The Context](context.md)
- [Providers](providers.md)
- [Mappers](mappers.md)
