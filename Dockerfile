# syntax=docker/dockerfile:1.4

ARG PHP_VERSION="8.3"
ARG CONTAINER_PROXY
ARG PROD_BUILD_IMAGE=build
ARG DEV_BUILD_IMAGE=build-dev

FROM ${CONTAINER_PROXY}php:$PHP_VERSION-alpine AS php
FROM ${CONTAINER_PROXY}mlocati/php-extension-installer:2.1 AS php-extension-installer
FROM ${CONTAINER_PROXY}composer:2.6 AS composer

FROM php

COPY --link --from=php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; install-php-extensions xdebug;
ENV XDEBUG_MODE=off

RUN set -eux; \
    apk add --no-cache bash git;

WORKDIR /app

ARG USER_NAME=app_user
ENV USER_NAME=$USER_NAME
ARG USER_UID=1000
ARG USER_GID=1000

RUN set -eux; \
    addgroup -g $USER_GID $USER_NAME; \
    adduser -D -u $USER_UID -G $USER_NAME -s /bin/sh $USER_NAME; \
    chown -R $USER_UID:$USER_GID .;

COPY --link --from=composer /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_HOME="/home/$USER_NAME/.composer"
ENV PATH="$PATH:$COMPOSER_HOME/vendor/bin"

ARG USER_UID=1000
ARG USER_GID=1000
