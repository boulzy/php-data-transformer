# 🧙 boulzy/data-transformer

_The_ boulzy/data-transformer _library provides a simple and flexible way to transform objects into other objects._

-----

## 📋 Table of contents

- [**Introduction**](#-introduction)
- [**Requirements**](#-requirements)
- [**Documentation**](#-documentation)
- [**Installation**](#-installation)
- [**Usage**](#-usage)

## 👋 Introduction

When manipulating data, the need to change the representation of this data sometimes arises, so it can be used by the
different components of the application. A common example is the need to transform DTOs to business objects and vice
versa.

This transformation often involves a lot of boilerplate code that is not directly related to the business logic of the
application. This library aims to reduce the repetition of this code by providing a simple and flexible way to transform
an object into an object of another class.

## ⚙️ Requirements

- PHP ^8.3
- symfony/property-access ^7.0
- symfony/property-info ^7.0
- phpdocumentor/reflection-docblock ^5.3

> ℹ️ This library also provides a bundle to use and extend the DataTransformer in a Symfony application.

## 📚 Documentation

The documentation is available in the [`docs/`](docs/getting-started.md) directory of this repository.

## ⬇️ Installation

The installation is done using [Composer](https://getcomposer.org/).

```bash
composer require boulzy/data-transformer
```

Don't forget to include `vendor/autoload.php` file.

## 🧑‍💻 Usage

First, you must retrieve an instance of `\Boulzy\DataTransfomer\DataTransformerInterface`.

If you're not in a Symfony application, check the [documentation](docs/getting-started.md) to learn how to get a
DataTransformer object.

The DataTransformer exposes one method: `transform(object $source, string $targetClass): object`. The first argument is
the object you currently have, and the second the class in which you want to transform it.

```php
<?php

namespace App\Controller;

use App\DTO\EditUser;
use App\Entity\User;
use Boulzy\DataTransformer\DataTransformerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController
{
    public function __construct(
        private ValidatorInterface $validator,
        private DataTransformerInterface $transformer,
        private ObjectManager $manager,
    ) {
    }

    public function __invoke(EditUser $editUser): User
    {
        $errors = $this->validator->validate($editUser);
        if (0 < \count($errors)) {
            throw new UnprocessableEntityHttpException((string) $errors);
        }

        $user = $this->transformer->transform($editUser, User::class);
        $this->manager->flush();

        return $user;
    }
}

```

Ready for more? [**Let's get started! 🚀**](docs/getting-started.md)
