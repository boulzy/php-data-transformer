<?php

namespace Boulzy\DataTransformer;

use Boulzy\DataTransformer\Exception\InvalidArgumentException;
use Boulzy\DataTransformer\Mapper\Mapper;
use Boulzy\DataTransformer\Provider\Provider;

final class DataTransformer implements DataTransformerInterface
{
    public function __construct(
        private Provider $provider,
        private Mapper $mapper,
    ) {
    }

    public function transform(object $data, object|string $target, array $context = []): object
    {
        $context[Context::INITIAL_DATA] ??= $data;

        if (\is_object($target)) {
            return $this->mapper->map($data, $target, $context);
        }

        if (!\class_exists($target)) {
            throw new InvalidArgumentException("Class \"{$target}\" does not exist");
        }

        return $this->mapper->map($data, $this->provider->load($target, $context), $context);
    }
}
