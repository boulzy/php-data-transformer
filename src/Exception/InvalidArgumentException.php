<?php

namespace Boulzy\DataTransformer\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements Exception
{
}
