<?php

namespace Boulzy\DataTransformer\Exception;

use Boulzy\DataTransformer\Exception\Exception;
use Boulzy\DataTransformer\Exception\RuntimeException;

class InvalidPropertyTypeException extends MapperException implements Exception
{
}
