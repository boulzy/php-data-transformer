<?php

namespace Boulzy\DataTransformer\Exception;

use Boulzy\DataTransformer\Exception\Exception;
use Boulzy\DataTransformer\Exception\RuntimeException;

class MapperException extends RuntimeException implements Exception
{

}