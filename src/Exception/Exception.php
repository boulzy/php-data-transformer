<?php

namespace Boulzy\DataTransformer\Exception;

interface Exception extends \Throwable
{
}
