<?php

namespace Boulzy\DataTransformer\Exception;

class InitialDataRequiredException extends RuntimeException implements Exception
{
}
