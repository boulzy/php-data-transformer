<?php

namespace Boulzy\DataTransformer\Exception;

class ProviderException extends RuntimeException implements Exception
{
}
