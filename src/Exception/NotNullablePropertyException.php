<?php

namespace Boulzy\DataTransformer\Exception;

class NotNullablePropertyException extends MapperException implements Exception
{
}
