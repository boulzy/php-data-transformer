<?php

namespace Boulzy\DataTransformer\Exception;

class InvalidPropertyMapException extends MapperException implements Exception
{
}
