<?php

namespace Boulzy\DataTransformer\Exception;

class RuntimeException extends \RuntimeException implements Exception
{
}