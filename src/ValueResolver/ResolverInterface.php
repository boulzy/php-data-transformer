<?php

namespace Boulzy\DataTransformer\ValueResolver;

interface ResolverInterface
{
    /**
     * @param mixed $source
     * @param string $targetType
     * @return mixed
     */
    public function resolve(mixed $source, string $targetType): mixed;
    public function supports(string $sourceType, string $targetType): bool;
}
