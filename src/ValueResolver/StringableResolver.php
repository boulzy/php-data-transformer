<?php

namespace Boulzy\DataTransformer\ValueResolver;

class StringableResolver implements ResolverInterface
{
    /**
     * @param object|string $source
     *
     * @return object|string
     */
    public function resolve(mixed $source, string $targetType): mixed
    {
        if (\is_string($source)) {
            $method = 'fromString';

            return $targetType::$method($source);
        }

        if ($source instanceof \Stringable) {
            return (string) $source;
        }

        return $source->toString();
    }

    public function supports(string $sourceType, string $targetType): bool
    {
        if ('string' === $sourceType) {
            return \is_callable([$targetType, 'fromString']);
        }

        if ('string' === $targetType) {
            return \is_subclass_of($sourceType, \Stringable::class) || \method_exists($sourceType, 'toString');
        }

        return false;
    }
}