<?php

namespace Boulzy\DataTransformer;

use Boulzy\DataTransformer\Exception\InvalidArgumentException;

/**
 * A DataTransformer is responsible for transforming objects to other objects.
 * This is particularly useful when working with DTOs.
 */
interface DataTransformerInterface
{
    /**
     * Transforms an object into another.
     *
     * @template T of object
     *
     * @param object $data The object to transform.
     * @param object|class-string<T> $target The target object or its class name.
     * @param array<string, mixed> $context Additional context to use when transforming the object.
     *
     * @return object<T> The transformed object.
     *
     * @throws InvalidArgumentException If the target class is not a valid class name.
     */
    public function transform(object $data, object|string $target, array $context = []): object;
}
