<?php

namespace Boulzy\DataTransformer;

/**
 * Context keys internally used.
 */
interface Context
{
    public const string INITIAL_DATA = 'initial_data';
    public const string PROPERTY_MAP = 'property_map';
}
