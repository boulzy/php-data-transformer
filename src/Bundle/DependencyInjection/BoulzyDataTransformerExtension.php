<?php

namespace Boulzy\DataTransformer\Bundle\DependencyInjection;

use Boulzy\DataTransformer\ValueResolver\ResolverInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class BoulzyDataTransformerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new XmlFileLoader(
            $container,
            new FileLocator(\dirname(__DIR__, 3).'/config'),
        );

        $loader->load('services.xml');

        $container
            ->registerForAutoconfiguration(ResolverInterface::class)
            ->addTag('boulzy_data_transformer.value_resolver')
        ;
    }
}