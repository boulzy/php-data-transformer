<?php

namespace Boulzy\DataTransformer\Bundle;

use Boulzy\DataTransformer\Bundle\DependencyInjection\BoulzyDataTransformerExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class BoulzyDataTransformerBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new BoulzyDataTransformerExtension();
    }
}
