<?php

namespace Boulzy\DataTransformer\Mapper;

use Boulzy\DataTransformer\Exception\MapperException;

/**
 * A mapper is responsible for mapping the data from a source object to a target object.
 */
interface Mapper
{
    /**
     * Maps the data from a source object to a target object.
     *
     * @param object $source The source object.
     * @param object $target The target object.
     * @param array<string, mixed> $context Additional context to use when mapping the data.
     *
     * @return object The target object.
     *
     * @throws MapperException If an error occurs during the mapping process.
     */
    public function map(object $source, object $target, array $context = []): object;
}
