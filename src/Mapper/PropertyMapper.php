<?php

namespace Boulzy\DataTransformer\Mapper;

use Boulzy\DataTransformer\Exception\InvalidPropertyMapException;
use Boulzy\DataTransformer\Exception\NotNullablePropertyException;
use Boulzy\DataTransformer\ValueResolver\ResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\PropertyInfo\Type;

class PropertyMapper implements Mapper
{
    public function __construct(
        private PropertyInfoExtractorInterface $propertyInfoExtractor,
        private PropertyAccessorInterface $propertyAccessor,
        /**
         * @var iterable<ResolverInterface>
         */
        private iterable $valueResolvers,
    ) {
    }

    public function map(object $source, object $target, array $context = []): object
    {
        $propertyMap = $this->getPropertyMap($source, $target, $context);
        foreach ($propertyMap as $sourceProperty => $targetProperty) {
            try {
                $value = $this->resolvePropertyValue($sourceProperty, $source, $targetProperty, $target, $context);
            } catch (NotNullablePropertyException) {
                continue;
            }

            $this->propertyAccessor->setValue($target, $targetProperty, $value);
        }

        return $target;
    }

    /**
     * @param array{property_map?: array<string, string>} $context
     *
     * @return string[]
     */
    private function getPropertyMap(object $source, object $target, array $context = []): array
    {
        if (null === $propertyMap = $context['property_map'] ?? null) {
            $propertyMap = [];

            foreach ($this->propertyInfoExtractor->getProperties($target::class) ?? [] as $property) {
                if (
                    $this->propertyAccessor->isWritable($target, $property)
                    && $this->propertyAccessor->isReadable($source, $property)
                ) {
                    $propertyMap[$property] = $property;
                }
            }

            return $propertyMap;
        }

        if (!\is_array($propertyMap)) {
            throw new InvalidPropertyMapException('The "property_map" context attribute must be an array.');
        }

        foreach ($propertyMap as $sourceProperty => $targetProperty) {
            if (!$this->propertyInfoExtractor->isWritable($target::class, $targetProperty)) {
                $targetClass = \get_class($target);
                throw new InvalidPropertyMapException("Property \"{$targetProperty}\" does not exist or is not writable on class {$targetClass}");
            }

            if (!$this->propertyInfoExtractor->isReadable($source::class, $sourceProperty)) {
                $sourceClass = \get_class($source);
                throw new InvalidPropertyMapException("Property \"{$sourceProperty}\" does not exist or is not readable on class {$sourceClass}");
            }
        }

        return $propertyMap;
    }

    private function resolvePropertyValue(string $sourceProperty, object $source, string $targetProperty, object $target, array $context = []): mixed
    {
        $sourceValue = $this->propertyAccessor->getValue($source, $sourceProperty);
        if (null === $targetTypes = $this->propertyInfoExtractor->getTypes($target::class, $targetProperty)) {
            return $sourceValue;
        }

        $sourceType = \get_debug_type($sourceValue);
        foreach ($targetTypes as $targetType) {
            if ('object' === $targetType->getBuiltinType()) {
                $targetType = $targetType->getClassName();
            } else {
                $targetType = $targetType->getBuiltinType();
            }

            if ($sourceType === $targetType || \is_subclass_of($sourceType, $targetType)) {
                return $sourceValue;
            }

            foreach ($this->valueResolvers as $resolver) {
                if ($resolver->supports($sourceType, $targetType)) {
                    return $resolver->resolve($sourceValue, $targetType);
                }
            }
        }

        if (null === $sourceValue) {
            $nullable = 0 < \count(\array_filter(
                $targetTypes,
                fn (Type $type) => $type->isNullable(),
            ));

            if (!$nullable) {
                $targetClass = \get_class($target);
                throw new NotNullablePropertyException("Property \"{$targetProperty}\" of class {$targetClass} is not nullable.");
            }

            return null;
        }

        $sourceClass = $source::class;
        $targetClass = $target::class;

        throw new InvalidPropertyMapException("The type of property \"{$sourceProperty}\" of {$sourceClass} does not match the type(s) of property \"{$targetProperty}\" of class {$targetClass}.");
    }
}