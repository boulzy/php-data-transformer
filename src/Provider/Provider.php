<?php

namespace Boulzy\DataTransformer\Provider;

use Boulzy\DataTransformer\Exception\InitialDataRequiredException;
use Boulzy\DataTransformer\Exception\ProviderException;

/**
 * A provider is responsible for loading a target object in its default state.
 */
interface Provider
{
    /**
     * Loads an object.
     *
     * @template T of object
     *
     * @param class-string<T> $className The class name of the object to load.
     * @param array<string, mixed> $context Additional context to use when loading the object.
     *
     * @return object<T> The loaded object.
     *
     * @throws InitialDataRequiredException If the initial data are not enough to load a new object.
     * @throws ProviderException If an error occurs during the loading process.
     */
    public function load(string $className, array $context = []): object;
}
