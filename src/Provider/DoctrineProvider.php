<?php

namespace Boulzy\DataTransformer\Provider;

use Boulzy\DataTransformer\Context;
use Boulzy\DataTransformer\Exception\InitialDataRequiredException;
use Boulzy\DataTransformer\Exception\ProviderException;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class DoctrineProvider implements Provider
{
    public function __construct(
        private Provider $inner,
        private ObjectManager $om,
        private PropertyAccessorInterface $propertyAccessor,
    ) {
    }

    public function load(string $className, array $context = []): object
    {
        try {
            $metadata = $this->om->getClassMetadata($className);
        } catch (MappingException) {
            $metadata = null;
        }

        if (null === $metadata || !isset($context[Context::INITIAL_DATA])) {
            return $this->inner->load($className, $context);
        }

        $identifier = $this->guessDoctrineIdentifiers($metadata, $context[Context::INITIAL_DATA]);
        if (null === $object = $this->om->find($className, $identifier)) {
            return $this->inner->load($className, $context);
        }

        return $object;
    }

    private function guessDoctrineIdentifiers(ClassMetadata $metadata, object $initialData): mixed
    {
        $identifiers = [];
        if (empty($doctrineIdentifiers = $metadata->getIdentifier())) {
            throw new ProviderException("Class {$metadata->getName()} does not have any identifier");
        }

        foreach ($doctrineIdentifiers as $doctrineIdentifier) {
            if (!$this->propertyAccessor->isReadable($initialData, $doctrineIdentifier)) {
                $initialDataType = \get_class($initialData);
                throw new InitialDataRequiredException("Cannot guess Doctrine identifier \"{$doctrineIdentifier}\" from {$initialDataType}");
            }

            $identifiers[$doctrineIdentifier] = $this->propertyAccessor->getValue($initialData, $doctrineIdentifier);
        }

        if (\count($identifiers) === 1) {
            return \reset($identifiers);
        }

        return $identifiers;
    }
}