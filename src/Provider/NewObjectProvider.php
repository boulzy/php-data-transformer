<?php

namespace Boulzy\DataTransformer\Provider;

use Boulzy\DataTransformer\Context;
use Boulzy\DataTransformer\Exception\InitialDataRequiredException;
use Boulzy\DataTransformer\Exception\ProviderException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class NewObjectProvider implements Provider
{
    public function __construct(
        private PropertyAccessorInterface $propertyAccessor,
    ) {
    }

    public function load(string $className, array $context = []): object
    {
        try {
            return new $className(...$this->getConstructorArguments($className, $context));
        } catch (\ArgumentCountError $e) {
            throw new InitialDataRequiredException(message: "Constructor of class \"{$className}\" has required parameters", previous: $e);
        } catch (\Error $e) {
            throw new ProviderException(message: "Class \"{$className}\" cannot be instantiated", previous: $e);
        }
    }

    private function getConstructorArguments(string $className, array $context): array
    {
        $initialData = $context[Context::INITIAL_DATA] ?? null;
        $reflection = new \ReflectionClass($className);
        $constructor = $reflection->getConstructor();

        if (!\is_object($initialData) || null === $constructor || 0 === $constructor->getNumberOfParameters()) {
            return [];
        }

        $arguments = [];
        foreach ($constructor->getParameters() as $parameter) {
            if (
                $parameter->isOptional()
                || $parameter->isDefaultValueAvailable()
                || !$this->propertyAccessor->isReadable($initialData, $parameter->getName())
            ) {
                continue;
            }

            $arguments[$parameter->getName()] = $this->propertyAccessor->getValue($initialData, $parameter->getName());
        }

        return $arguments;
    }
}
